﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TakeSnapShot : MonoBehaviour
{
    public static TakeSnapShot instance;

    Camera mycam;
    public bool firstShot;
    public RenderTexture finalRenderTex;

    //static Material _outputMat;
    //static Material _outputMatDrawn;
    //static Material OutputMat
    //{
    //    get
    //    {
    //        if (!_outputMat) _outputMat = UICanvasManager.instance.OutputMatObj.GetComponent<MeshRenderer>().material;
    //        return _outputMat;
    //    }
    //}
    //static Material OutputMatDrawn
    //{
    //    get
    //    {
    //        if (!_outputMatDrawn) _outputMatDrawn = UICanvasManager.instance.OutputMatObjDrawn.GetComponent<MeshRenderer>().material;
    //        return _outputMatDrawn;
    //    }
    //}
    Sprite refImageSprite;
    Sprite DrawnImageSprite;
    public Texture2D whiteTex;

    public Image refImageUI;
    public Image finalImageUI;

    //bool snapped;
    int ajaira;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
        mycam = GetComponent<Camera>();
    }
    //void Start()
    //{
    //    OutputMatObj = UICanvasManager.instance.OutputMatObj;
    //    OutputMatObjDrawn = UICanvasManager.instance.OutputMatObjDrawn;
    //}

    // Update is called once per frame
    void Update()
    {


        //if ( Input.GetKeyDown(KeyCode.Space))
        //{
        //    //if (!firstShot)
        //    //{
        //    //    TakeScreenShot(OutputMatObj);
        //    //    firstShot = true;
        //    //}
        //    //else
        //    //{
        //    //    TakeScreenShot(OutputMatObjDrawn);
        //    //}
        //
        //    //Debug.Log("spaced");
        //    TakeFirstSnap();
        //    TakeLastSnap();
        //}

        //if (ajaira == 2)
        //{
        //    TakeFirstSnap();
        //    //snapped = true;
        //    ajaira += 1;
        //    Debug.Log("snapped11");
        //}
        //else if(ajaira < 2)
        //{
        //    ajaira += 1;
        //}
        
    }

    public Texture2D TakeScreenShot( Image refImageUI)
    {

        RenderTexture currentActiveRT = RenderTexture.active;
        RenderTexture.active = finalRenderTex;
        mycam.Render();
        var texture2D = new Texture2D(finalRenderTex.width, finalRenderTex.height);
        texture2D.ReadPixels(new Rect(0, 0, finalRenderTex.width, finalRenderTex.height), 0, 0);
        texture2D.Apply();
        refImageUI.sprite = Sprite.Create(texture2D, new Rect(0, 0, finalRenderTex.width, finalRenderTex.height), new Vector2(0.5f, 0.5f));
        
        RenderTexture.active = currentActiveRT;

        return texture2D;
    }
    //public Texture2D TakeScreenShot(Material mat , Image refImageUI)
    //{
    //    
    //    RenderTexture currentActiveRT = RenderTexture.active;
    //
    //    RenderTexture.active = finalRenderTex;
    //
    //    var texture2D = new Texture2D(finalRenderTex.width, finalRenderTex.height);
    //
    //    
    //
    //    texture2D.ReadPixels(new Rect(0, 0, finalRenderTex.width, finalRenderTex.height), 0, 0);
    //
    //    texture2D.Apply();
    //    //refImageSprite = Sprite.Create(texture2D, new Rect(0, 0, finalRenderTex.width, finalRenderTex.height), new Vector2(0.5f, 0.5f) );
    //
    //    //refImage = (Sprite)texture2D;
    //    refImageUI.sprite = Sprite.Create(texture2D, new Rect(0, 0, finalRenderTex.width, finalRenderTex.height), new Vector2(0.5f, 0.5f));
    //
    //
    //    //var byteArray = texture2D.EncodeToPNG();
    //
    //    //System.IO.File.WriteAllBytes(Application.dataPath + "/outPutTextures/camScreenShot.png", byteArray);
    //    //Debug.Log(Application.dataPath + "/outPutTextures/camScreenShot.png");
    //
    //    //if (mat)
    //    //{
    //    //    mat.EnableKeyword("_MainTex");
    //    //    mat.SetTexture("_MainTex", texture2D);
    //    //}
    //    //UnityEngine.Object.Destroy(texture2D);
    //
    //    RenderTexture.active = currentActiveRT;
    //
    //    return texture2D;
    //}
    static Texture2D savedTextureforFirstMat;
    static bool firstTime=true;
    static void SetTexture(Material mat, Texture2D texture2D)
    {
        mat.EnableKeyword("_MainTex");
        mat.SetTexture("_MainTex", texture2D);
    }
    //public static void SetSavedTexture()
    //{
    //    if(savedTextureforFirstMat)
    //        SetTexture(OutputMat, savedTextureforFirstMat);
    //
    //}

    //public void TakeFirstSnap()
    //{
    //         
    //   savedTextureforFirstMat =  TakeScreenShot(null);
    //    if (firstTime)
    //    {
    //        firstTime = false;
    //        SetSavedTexture();
    //
    //    }
    //}
    

    public void TakeLastSnap()
    {
        PissRenderCam.pissCam.Render();
        
        TakeScreenShot( finalImageUI);
        //Invoke("AjairaDelay", 1f);
    }
    public void TakeFirstSnap()
    {
        TakeScreenShot( refImageUI);
        EraseFinalSnap();
    }

    public void EraseFinalSnap()
    {
        //TakeScreenShot(OutputMatDrawn);
        //OutputMatDrawn.EnableKeyword("_MainTex");
        //OutputMatDrawn.SetTexture("_MainTex", whiteTex);
        finalImageUI.sprite = Sprite.Create(whiteTex, new Rect(0, 0, finalRenderTex.width, finalRenderTex.height), new Vector2(0.5f, 0.5f));
    }


    //public void AjairaDelay()
    //{
    //    TakeScreenShot(OutputMatDrawn);
    //    ImageManager.instance.ChangeImage(GameManage.instance. levelImageMats[GameManage.instance. level]);
    //}


}
