﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationFuntionality : MonoBehaviour
{
    public GameObject Visuals;
    public GameObject Vitals;

    public TakeSnapShot snapShotCam;

    
    
    // Start is called before the first frame update
    void Start()
    {
        visualEnable();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PissingEnable()
    {
        Vitals.SetActive(true);
        Visuals.SetActive(true);
    }

    public void visualEnable()
    {
        Vitals.SetActive(false);
        Visuals.SetActive(true);
    }

    //public void TakeRefSnap()
    //{

    //    Debug.Log("ref snap");
    //    snapShotCam.TakeFirstSnap();
    //}

    //public void TakeFinalSnap()
    //{
    //    snapShotCam.TakeLastSnap();
    //}
}
