﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManage : MonoBehaviour
{
    public static GameManage instance;
    public GameObject stationAlive;
    public Transform[] levelSpawnPoints;
    public Material[] levelImageMats;
    public int level;
    public Texture[] images;
    public Transform initialPoint;
    //public GameObject image
    public Material imageMat;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
        level = PlayerPrefs.GetInt("level", 0);
    }
    void Start()
    {
        //imageMat
        level = 0;
        PlayerPrefs.SetInt("level", level);
        DetermineStartPoint(level);

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NextStation()
    {
        UpdateLevel();
        stationAlive.transform.position = levelSpawnPoints[level].position;
        stationAlive.transform.rotation = levelSpawnPoints[level].rotation;

        ImageManager.instance.ChangeImage(levelImageMats[level]);
    }

    public void UpdateLevel()
    {
        level += 1;
        PlayerPrefs.SetInt("level", level);
    }

    public void DetermineStartPoint(int level)
    {
        Vector3 pos = (level == 0) ? initialPoint.position : levelSpawnPoints[level - 1].position;
        Quaternion rot = (level == 0) ? initialPoint.rotation : levelSpawnPoints[level - 1].rotation;

        PlayerController.instance.transform.position = pos;
        PlayerController.instance.transform.rotation = rot;

        PlayerController.instance.targetPos = pos;
        PlayerController.instance.targetRot = rot;

        pathFollowing.instance.gameObject.transform.position = pos;
        pathFollowing.instance.gameObject.transform.rotation = rot;

        //if (level == 0)
        //{
        //    PlayerController.instance.transform.position = initialPoint.position;
        //    PlayerController.instance.transform.rotation = initialPoint.rotation;

        //    PlayerController.instance.targetPos = initialPoint.position;
        //    PlayerController.instance.targetRot = initialPoint.rotation;

        //    pathFollowing.instance.gameObject.transform.position = initialPoint.position;
        //    pathFollowing.instance.gameObject.transform.rotation = initialPoint.rotation;
        //}
        //else if(level > 0)
        //{
        //    PlayerController.instance.transform.position         = levelSpawnPoints[level - 1].position;
        //    PlayerController.instance.transform.rotation         = levelSpawnPoints[level - 1].rotation;
        //    PlayerController.instance.targetPos                  = levelSpawnPoints[level - 1].position;
        //    PlayerController.instance.targetRot                  = levelSpawnPoints[level - 1].rotation;
        //    pathFollowing.instance.gameObject.transform.position = levelSpawnPoints[level - 1].position;
        //    pathFollowing.instance.gameObject.transform.rotation = levelSpawnPoints[level - 1].rotation;
        //}

        stationAlive.transform.position = levelSpawnPoints[level].position;
        stationAlive.transform.rotation = levelSpawnPoints[level].rotation;

        ImageManager.instance.ChangeImage(levelImageMats[level]);
    }
}
