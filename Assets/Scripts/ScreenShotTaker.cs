﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenShotTaker : MonoBehaviour
{
    public static ScreenShotTaker instance;
    public ParticleSystem penParticle;
    public ParticleSystem yellowPenParticle;
    public Vector3 particlePos;
    public Camera rayCam;
    public Vector3 dummyPos;
    float emissionRate;
    public bool goEmit;
    public Button resetButton;
    public ParticleSystem smokeParticle;

    public GameObject waterPot;
    public ParticleSystem waterFall;
    float tilt;

    public float upOffset;
    public Vector3 mouseOffset;

    float pissingTimer;

    public float maxPissingTime;

    public Slider greenSlider;
    bool getOffset;

    public float screenHeight;

    Vector3 oldPos;
    Vector3 cursorVel;

    public float cursorCatchSpeed;

    public Transform parentPlayer;
    public WaterFallEffectManager waterfallEffect;

    public float pauseTimer;
    public bool Pause;

    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        //dummyPos = penParticle.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        //Application.targetFrameRate = 60;
        //penParticle.transform.localPosition = Vector3.Lerp(penParticle.transform.localPosition,particlePos, Time.deltaTime * 20);
        //penParticle.transform.localPosition = particlePos;
        //waterPot.transform.localRotation = Quaternion.Euler(0, tilt, 0);

        Raycasting();
        //CursorVelocityCounter();
        var emission = penParticle.emission;
        var yellowEmission = yellowPenParticle.emission;
        var smokeEmission = smokeParticle.emission;
        emission.rateOverDistance = yellowEmission.rateOverDistance = emissionRate * 2;
        //emission.rateOverTime = emissionRate / 5;
        smokeEmission.rateOverDistance = emissionRate/4;
        waterFall.emissionRate = 80 * emissionRate;
        

        greenSlider.value = pissingTimer / maxPissingTime;
        if (goEmit)
        {
            
            //penParticle.transform.localPosition = Vector3.Lerp(penParticle.transform.localPosition, particlePos, Time.deltaTime * 20);
            emissionRate = 10;
            //tilt = Mathf.Lerp(tilt, 50 , Time.deltaTime * 15f);


            
        }
        else
        {
            
            //penParticle.transform.localPosition = particlePos;
            emissionRate = 0;
            //tilt = Mathf.Lerp(tilt, 0, Time.deltaTime * 15f);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetParticle();
        }
        



    }

    public void Raycasting()
    {
        if (Input.GetMouseButton(0))
        {
            pauseTimer = 0;
            
            if (Pause)
            {
                Pause = false;
                penParticle.Play();
                yellowPenParticle.Play();
            }
            //penParticle.Play();
            RaycastHit hit;
            //screenHeight = rayCam.pixelHeight;
            screenHeight = Screen.height;
            Vector3 mousePos = Input.mousePosition + new Vector3(0, upOffset * screenHeight / 100 , 0);
            //Vector3 mousePos = Input.mousePosition + mouseOffset;
            //Vector3 mousePos = Input.mousePosition;

            var ray = rayCam.ScreenPointToRay(mousePos);
            //var ray = Camera.main.ScreenPointToRay(mousePos);

            if (Physics.Raycast(ray, out hit))
            {
                //if (!getOffset)
                //{
                //    getOffset = true;
                //    mouseOffset = -particlePos + (hit.point);
                //    mouseOffset.y = 0;
                //}
                if (Mathf.Abs(particlePos.x) <= 4.5f && Mathf.Abs(particlePos.z) <= 4.5f)
                {
                    //particlePos = (hit.point) - mouseOffset;
                    //dummyPos = hit.point - parentPlayer.position;
                    dummyPos =transform.InverseTransformPoint( hit.point) ;
                    if (!goEmit)
                    {
                        particlePos = dummyPos;
                    }
                    particlePos = Vector3.Lerp(particlePos, dummyPos, Time.deltaTime * cursorCatchSpeed);
                    //particlePos = (hit.point);
                    smokeParticle.transform.localPosition = particlePos;
                    Debug.DrawRay(hit.point, transform.up, Color.green);
                    //dummyPos = particlePos;
                    penParticle.transform.localPosition = particlePos;
                    yellowPenParticle.transform.localPosition = particlePos;
                    //penParticle.transform.localPosition = Vector3.Lerp(penParticle.transform.localPosition, particlePos, Time.deltaTime * 10);
                    pissingTimer += Time.deltaTime;
                    if (pissingTimer > maxPissingTime)
                    {
                        //Invoke("GoEmit", 0.5f);
                        GoEmit();
                    }

                    //GoEmit();

                    //waterfallEffect.hitPoint = particlePos ;
                    waterfallEffect.hitPoint = (hit.point);
                    
                }
                else if (hit.transform.gameObject.layer == 9 || hit.transform.gameObject.layer == 10)
                {
                    //particlePos = (hit.point) - mouseOffset;
                    //dummyPos = hit.point - parentPlayer.position;
                    dummyPos = transform.InverseTransformPoint(hit.point);
                    particlePos = Vector3.Lerp(particlePos, dummyPos, Time.deltaTime * cursorCatchSpeed);
                    //particlePos = (hit.point);
                    smokeParticle.transform.localPosition = particlePos;
                    Debug.DrawRay(hit.point, transform.up, Color.green);
                    //dummyPos = particlePos;
                    penParticle.transform.localPosition = particlePos;
                    //penParticle.transform.localPosition = Vector3.Lerp(penParticle.transform.localPosition, particlePos, Time.deltaTime *10);
                    //GoEmit();
                    goEmit = false;
                    pissingTimer = 0;

                    //waterfallEffect.hitPoint = particlePos ;

                    waterfallEffect.hitPoint =(hit.point);
                }
                //else if(Mathf.Abs( particlePos.x ) <= 4.5f && Mathf.Abs(particlePos.z) <= 4.5f)
                //{
                //
                //}




            }
            else
            {
                //emissionRate = 0;
            }
        }
        else
        {
            goEmit = false;
            pissingTimer = 0;
            getOffset = false;
            ManageParticlePause();
        }
    }

    public void ManipulateParticles()
    {
        dummyPos = new Vector3(particlePos.x , dummyPos.y , particlePos.z);
        
    }

    public void GoEmit()
    {
        if (!goEmit)
        {
            goEmit = true;
        }
    }

    public void ResetParticle()
    {
        penParticle.Clear();
        yellowPenParticle.Clear();
    }


    public void CursorVelocityCounter()
    {
        cursorVel = particlePos - oldPos;
        oldPos = particlePos;
        //WaterFallEffectManager.instance.pissSideVel = cursorVel;
    }

    public void ManageParticlePause()
    {
        if (!Pause)
        {
            if (pauseTimer < 0.5f)
            {
                pauseTimer += Time.deltaTime;
            }
            else
            {
                Pause = true;
                penParticle.Pause();
                yellowPenParticle.Pause();
                //pauseTimer = 0;

            }
        }

        
        
    }



}
