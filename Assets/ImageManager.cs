﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageManager : MonoBehaviour
{
    public Material[] imagemats;
    public static ImageManager instance;
    //public Texture[] images;

    public int matIndex;
    public TakeSnapShot snapShotTaker;

    public bool firstSnapped;
    public Material whiteMat;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        //matIndex = PlayerController.instance.stationIndex;
        //this.GetComponent<Renderer>().material = imagemats[matIndex];
        //Invoke("ChangeImage" , 1f);
        //Debug.Log("Born");

        //ChangeImage(imagemats[GameManage.instance.level]);
    }

    // Update is called once per frame
    //void Update()
    //{
    //    if(Input.GetKeyDown(KeyCode.R))
    //    {
    //        RemoveDottedImage();
    //    }
    //}

    public void ChangeImage(Material mat)
    {
        this.GetComponent<Renderer>().material = mat;
        //Invoke("FirstSnap", 0.2f);
        //snapShotTaker.TakeFirstSnap();
    }

    public void RemoveDottedImage()
    {
        ChangeImage(whiteMat);
        

    }

    //void FirstSnap()
    //{
    //    Debug.Log("f snap");
    //    snapShotTaker.TakeFirstSnap();
    //}
}
