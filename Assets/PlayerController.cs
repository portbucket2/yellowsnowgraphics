﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;
    public bool atStation;
    public Transform snapTarget;
    public float SnapSpeed;
    public Vector3 targetPos;
    public Quaternion targetRot;
    public GameObject camholder;
    float camTiltRot;
    public float camTilt;
    public float camTiltmax;
    public Animator HeadAnim;

    public bool GameOn;
    public bool PissingOn;
    public GameObject station;
    public StationFuntionality currStationFun;

    //public GameObject[] Stations;
    public int stationIndex;
    public float walkSpeed;
    public GameObject followTarget;
    public float firstSnapTakingAllowed;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        //for (int i = 0; i < Stations.Length; i++)
        //{
        //    Stations[i].SetActive(false);
        //}
        //stationIndex = - 1;

        if(snapTarget == null)
        {
            snapTarget = camholder.transform;
        }
    }
    void Start()
    {
        //targetPos = this.transform.position;
 
        //WakeUpStation(stationIndex);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameOn)
        {
            camholder.transform.localRotation = Quaternion.Euler(new Vector3(camTiltRot, 0, 0));
            this.transform.position = Vector3.Lerp(this.transform.position, targetPos, Time.deltaTime * 2);
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetRot, Time.deltaTime * 2);
            if (atStation)
            {
                SnapToPoint(snapTarget);


                camTiltRot = Mathf.Lerp(camTiltRot, camTilt, Time.deltaTime * 2);
            }
            else
            {
                camTiltRot = Mathf.Lerp(camTiltRot, 0, Time.deltaTime);
                //targetPos.z += walkSpeed * Time.deltaTime * 50;
                targetPos = followTarget.transform.position;
                targetRot = followTarget.transform.rotation;
                //this.transform.Translate(0, 0, 0.05f * Time.deltaTime * 50);
                //targetPos = this.transform.position;
            }

            if( Vector3.Distance(this.transform.position,snapTarget.position) < 0.2f && !PissingOn && snapTarget!= null)
            {
                PissingOn = true;
                currStationFun.PissingEnable();

                //station = currStationFun.gameObject;
                
                UICanvasManager.instance.GoToGamePanel();
                UICanvasManager.instance.ResetMatch ();
                //Invoke("Pause", 2f);
                Pause();

                TakeSnapShot.instance.TakeFirstSnap();
                
                
            }
            //else if(snapTarget != null)
            //{
            //    currStationFun.visualEnable();
            //}
            pathFollowing.instance.paused = false;
        }
        else
        {
            pathFollowing.instance.paused = true;
        }
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "stationRoot" && !atStation)
        {
            snapTarget = other.transform;
            //snapTarget.rotation = 
            atStation = true;
            currStationFun = other.gameObject.GetComponentInParent<StationFuntionality>();
            station = currStationFun.gameObject;
            HeadAnim.SetTrigger("HeadStill");
            
        }
        if (other.tag == "stationTerminal")
        {
            //ForGetLeftStation();
            

        }
    }

    public void SnapToPoint(Transform point)
    {
        targetPos = Vector3.MoveTowards(targetPos , point.position , SnapSpeed * Time.deltaTime * 50);
        targetRot = point.rotation;
        camTilt = Mathf.MoveTowards(camTilt, camTiltmax, SnapSpeed * Time.deltaTime * 50 * 7);

    }

    public void StartWalkAnim()
    {
        HeadAnim.SetTrigger("HeadWalk");
        
    }

    public void StartGame()
    {
        
        GameOn = true;
    }

    public void Pause()
    {
        GameOn = false;
        Debug.Log("PAUSEDDD");
    }

    //public void TakeFinalSnap()
    //{
    //    currStationFun.TakeFinalSnap();
    //}

    public void LeaveStation()
    {
        //CancelInvoke("Pause");
        snapTarget = camholder.transform;
        PissingOn = false;
        //ForGetLeftStation();
        ImageManager.instance.RemoveDottedImage();

        //TakeSnapShot.instance.TakeLastSnap();

        //GameManage.instance.NextStation();
        TakeSnapShot.instance.TakeLastSnap();
        GameManage.instance.NextStation();
        ScreenShotTaker.instance.ResetParticle();
        //StartCoroutine(AjairaDelayCoroutine());
        atStation = false;
        GameOn = true;
        
        StartWalkAnim();
        camTilt = 0;

        
        //Invoke("ForGetLeftStation", 0f);
        //snapTarget = null;

    }

    //public void ForGetLeftStation()
    //{
    //    station.SetActive(false);
    //    WakeUpStation(0);
    //}
    //
    //public void WakeUpStation(int index)
    //{
    //    
    //    stationIndex += 1;
    //    //Stations[stationIndex].SetActive(true);
    //    
    //}

    public void AjairaDelay()
    {
        TakeSnapShot.instance.TakeLastSnap();
        GameManage.instance.NextStation();
    }
    IEnumerator AjairaDelayCoroutine()
    {
        
        yield return new WaitForSeconds(0.1f);
        TakeSnapShot.instance.TakeLastSnap();
        GameManage.instance.NextStation();
        ScreenShotTaker.instance.ResetParticle();

        //yield break;

    }
}
