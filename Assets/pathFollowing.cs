﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pathFollowing : MonoBehaviour
{
    public GameObject target;
    public float AdvanceSpeed;
    public float RotSpeed;
    public float zFac;
    public int frames;
    public float yRot;
    public float tarYRot;

    public float finalYRot;
    float journey;
    public int stepsNeeded;
    Quaternion targetRot;

    public float steer;

    public Vector3 steerVector;

    bool GotInitials;
    public bool paused;
    public static pathFollowing instance;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        //if (!GotInitials)
        //{
        //    //CalculateInitials();
        //    GotInitials = true;
        //}
        if (!paused)
        {
            MoveAhead();
        }
        
        //if (transform.position == new Vector3(0, 0, 0))
        //{
        //    AdvanceSpeed = 0;
        //}
        //else
        //{
        //    frames += 1;
        //}
        
    }


    void MoveAhead()
    {
        if (target)
        {
            this.transform.Translate(0, 0, AdvanceSpeed*Time.deltaTime * 50f);

            transform.Rotate(0, steer * Time.deltaTime * 50f, 0);
            steer = steerVector.x * RotSpeed - steerVector.z * zFac;
            steerVector = transform.InverseTransformDirection(-this.transform.position + target.transform.position);
        }
        else
        {
            this.transform.Translate(0, 0, AdvanceSpeed * Time.deltaTime * 50f);
        }
        
    }

    //void CalculateInitials()
    //{
    //    
    //    //steerVector = transform.InverseTransformDirection(-this.transform.position + target.transform.position);
    //}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "pathPoint")
        {
            target = other.gameObject.GetComponent<pathPointBehavior>().nextPoint.gameObject;
            
            //GotInitials = false;
        }
        
    }
}
