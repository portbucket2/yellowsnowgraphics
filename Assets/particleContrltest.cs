﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class particleContrltest : MonoBehaviour
{
    ParticleSystem particle;
    // Start is called before the first frame update
    void Start()
    {
        particle = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            particle.Clear();
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            particle.Stop();
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            particle.Pause();
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            particle.Play();
        }
    }
}
