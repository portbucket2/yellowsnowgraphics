﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICanvasManager : MonoBehaviour
{
    public GameObject MenuPanel;
    public GameObject GamePanel;
    public GameObject MatchPanel;
    public static UICanvasManager instance;

    public Slider matchSlider;
    public Text matchValueText;
    float matchValue;
    public float maxMatchValue;

    public bool matchMake;
    public GameObject OutputMatObj;
    public GameObject OutputMatObjDrawn;

    public Button GameStartButton;
    public Button DrawingDoneButton;
    public Button MatchingNextButton;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        GoToMenuPanel();
        AddListenerToButtons();
    }

    // Update is called once per frame
    void Update()
    {
        if (matchMake && matchValue <= maxMatchValue)
        {
            matchValue += Time.deltaTime * 40f;
        }
        matchSlider.value = matchValue/100;
        matchValueText.text = "" + (int)matchValue + " %";
    }

    public void CloseAllPanels()
    {
        //TakeSnapShot.SetSavedTexture();
        //Debug.Log("Tex save called");
        MenuPanel.SetActive(false);
        GamePanel.SetActive(false);
        MatchPanel.SetActive(false);
    }

    public void GoToGamePanel()
    {
        MenuPanel.SetActive(false);
        GamePanel.SetActive(true);
        MatchPanel.SetActive(false);
    }

    public void GoToMatchPanel()
    {
        MenuPanel.SetActive(false);
        GamePanel.SetActive(false);
        MatchPanel.SetActive(true);
    }
    public void GoToMenuPanel()
    {
        MenuPanel.SetActive(true);
        GamePanel.SetActive(false);
        MatchPanel.SetActive(false);
    }

    public void Matchmake()
    {
        matchMake = true;
    }
    public void ResetMatch()
    {
        matchMake = false;
        matchValue = 0;
    }

    public void AddListenerToButtons()
    {
        GameStartButton.onClick.AddListener(CloseAllPanels);
        GameStartButton.onClick.AddListener(PlayerController.instance.StartGame);
        GameStartButton.onClick.AddListener(PlayerController.instance.StartWalkAnim);

        DrawingDoneButton.onClick.AddListener(GoToMatchPanel);
        DrawingDoneButton.onClick.AddListener(Matchmake);
        DrawingDoneButton.onClick.AddListener(PlayerController.instance.LeaveStation);
        //DrawingDoneButton.onClick.AddListener(PlayerController.instance.currStationFun.visualEnable);

        MatchingNextButton.onClick.AddListener(CloseAllPanels);
        MatchingNextButton.onClick.AddListener(ResetMatch);
        //MatchingNextButton.onClick.AddListener(PlayerController.instance.LeaveStation);
    }

    
}
